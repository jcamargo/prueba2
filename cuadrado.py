#la clase siempre se pone con la primera letra en mayuscula y en singulat
class Rectangulo:
    """Un ejemplo de clase para los cuadrados """ #Comentario para la clase
    
    def __init__(self,base,altura):#Que tiene el rectangulo? base y altura
        self.base=base 
        self.altura = altura
    def calculo_de_perimetro(self):
        perimetro = (2 *(self.base ))+ (2*(self. altura))
        return perimetro
    def calculo_area(self):
        area = self.base * self.altura
        return area
    def __str__(self): #Sirve para imrimir mensaje de los objetos.
        return " El rectangulo de base {base} y altura {altura}, su es {per} y su área es {ar}".format(base =self.base,altura =self.altura,per = self.calculo_de_perimetro(),ar =self.calculo_area())
class Cuadrado:
    """Un ejemplo de clase para los cuadrados """ #Comentario para la clase
    
    def __init__(self,l):#Que tiene el cuadrado?El lado
        self.lado = l
    def calculo_de_perimetro(self):
        perimetro = self.lado * 4
        return perimetro
    def calculo_area(self):
        area = self.lado ** 2
        return area
    def __str__(self): #Sirve para imrimir mensaje de los objetos.
        return "El cuadrado de lado {lado} ,su perimetro  es {per} y su área es {ar}".format(lado =self.lado, per = self.calculo_de_perimetro(),ar =self.calculo_area())
 









cuadrado1= Cuadrado(8)
rectangulo1 = Rectangulo(8,5)
print(rectangulo1)
print(cuadrado1)
print(rectangulo1.base)