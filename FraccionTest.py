from Fracción import Fraccion
import unittest
class MultiplyTestCase(unittest.TestCase):
    #Aqui estoy llamando a unittes para poder ejecutarlo y TestCase es un objeto preterminado de unites
    def test_suma01(self):
        f1 = Fraccion(1,2)
        f2 = Fraccion(1,2)
        suma = f1.suma(f2)
        
        self.assertNotEqual(suma.numerador,2)
        self.assertNotEqual(suma.denominador,5)

    def test_suma02(self):
        f1 = Fraccion(1,2)
        f2 = Fraccion(-1,2)
        suma = f1.suma(f2)
        
        self.assertNotEqual(suma.numerador,2)
        self.assertNotEqual(suma.denominador,0)

#Es una forma de iniciar el unitest main
unittest.main()