import unittest
from mul import multiply
from mul import add

class MultiplyTestCase(unittest.TestCase):
    #Aqui estoy llamando a unittes para poder ejecutarlo y TestCase es un objeto preterminado de unites
    def test_multiplication_with_correct_values(self):
        self.assertEqual(multiply(5, 5), 25)

    def test_multiplication_with_not_correct_values(self):
        mul2 = multiply(5,5)#Otra forma de expresarlo
        self.assertEqual(mul2, 24)
    def test_multiplication_with_in_correct_values(self):
        ad1 =add(5,5)#Otra forma de expresarlo
        self.assertNotEqual(ad1, 24)

#Es una forma de iniciar el unitest main
unittest.main()